<?php

/**
 * @file
 * Install file for webform2sftp module.
 */

// Always includes common file.
module_load_include('inc', 'webform2sftp', 'inc/webform2sftp.common');

/**
 * Implements hook_schema().
 */
function webform2sftp_schema() {
  $schema['webform2sftp_components_order'] = array(
    'description' => 'Store the order of the components to export.',
    'fields' => array(
      'id' => array(
        'description' => 'Unique ID',
        'type' => 'serial',
        'unsigned' => TRUE,
      ),
      'nid' => array(
        'description' => 'The webform nid',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'cid' => array(
        'description' => 'The webform component id',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'disabled' => array(
        'description' => 'If component should be exported or not.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'weight' => array(
        'description' => 'The component weight',
        'type' => 'int',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
  );

  $schema['webform2sftp_last_submission_id_to_sftp'] = array(
    'description' => 'Store the last submission id sent to sFTP.',
    'fields' => array(
      'nid' => array(
        'description' => 'The webform nid',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'webform2sftp_last_webform_submission_sid' => array(
        'type' => 'int',
        'default' => -1,
      ),
    ),
    'primary key' => array('nid'),
  );

  return $schema;
}

/**
 * Implements hook_schema_alter().
 */
function webform2sftp_schema_alter(&$schema) {
  if (isset($schema['webform'])) {
    $fields = _webform2sftp_get_webform_schema_fields();
    foreach ($fields as $field_name => $spec) {
      $schema['webform']['fields'][$field_name] = $spec;
    }
  }
}

/**
 * Implements hook_install().
 */
function webform2sftp_install() {
  $schema = drupal_get_schema('webform');
  $fields = _webform2sftp_get_webform_schema_fields();
  foreach ($fields as $field_name => $spec) {
    db_add_field('webform', $field_name, $schema['fields'][$field_name]);
  }

  webform2sftp_insert_components_order();
}

/**
 * Implements hook_uninstall().
 */
function webform2sftp_uninstall() {
  $fields = _webform2sftp_get_webform_schema_fields();
  foreach ($fields as $field_name => $spec) {
    db_drop_field('webform', $field_name);
  }

  // Deleting variables.
  db_delete('variable')->condition('name', 'webform2sftp_%', 'LIKE')->execute();
}

/**
 * Function to get webform2sftp fields to add in webform_schema.
 *
 * @return array
 *   The additional fields for webform schema.
 */
function _webform2sftp_get_webform_schema_fields() {
  $fields['webform2sftp_integration_flag'] = array(
    'type' => 'char',
    'default' => '0',
    'not null' => TRUE,
  );
  $fields['webform2sftp_encryption_flag'] = array(
    'type' => 'char',
    'default' => '0',
    'not null' => TRUE,
  );
  $fields['webform2sftp_submission_limit'] = array(
    'type' => 'int',
    'not null' => FALSE,
  );
  $fields['webform2sftp_file_delimiter'] = array(
    'type' => 'varchar',
    'length' => 255,
    'not null' => FALSE,
  );
  $fields['webform2sftp_file_name_prefix'] = array(
    'type' => 'varchar',
    'length' => 255,
    'not null' => FALSE,
  );

  return $fields;
}

/**
 * Insert default components order.
 */
function webform2sftp_insert_components_order() {
  module_load_include('inc', 'webform2sftp', 'inc/webform2sftp.common');

  $webforms = webform2sftp_get_webforms();
  if (!empty($webforms)) {
    foreach ($webforms as $webform) {
      // Insert webforms to "webform2sftp_last_submission_id_to_sftp" table.
      webform2sftp_insert_webforms_last_submission($webform->nid);

      $node = node_load($webform->nid);
      $weight = 0;
      foreach ($node->webform['components'] as $component) {
        $weight++;
        webform2sftp_insert_entry_components_order($component, $weight);
      }
    }
  }
}

/**
 * Insert webform nids to "webform2sftp_last_submission_id_to_sftp" table.
 */
function webform2sftp_insert_webforms_last_submission($nid) {
  db_insert('webform2sftp_last_submission_id_to_sftp')
    ->fields(array(
      'nid' => $nid,
      'webform2sftp_last_webform_submission_sid' => -1,
    ))
    ->execute();
}

/**
 * Implements hook_requirements().
 */
function webform2sftp_requirements($phase) {
  $requirements = array();

  if ($phase == 'install') {
    webform2sftp_check_module_requirements($requirements);
  }

  return $requirements;
}

/**
 * Checks install module requirements.
 */
function webform2sftp_check_module_requirements(&$requirements) {
  $t = get_t();

  // Check required extensions.
  $extensions = array(
    'ssh2',
    'gnupg',
  );

  foreach ($extensions as $extension) {
    $requirements[$extension . '_extension'] = array(
      'title' => $t('PHP ' . $extension . ' extension support.'),
    );

    if (!extension_loaded($extension)) {
      $warning_message = $t('The PHP extension ' . $extension . ' must be installed and configured on the server to module works.');
      $requirements[$extension . '_extension']['value'] = $t('Not installed');
      $requirements[$extension . '_extension']['severity'] = REQUIREMENT_WARNING;
      $requirements[$extension . '_extension']['description'] = $t('The PHP extension ' . $extension . ' must be installed and enabled on the server to continue with module installation.');
      if ('gnupg' == $extension) {
        $encryption_use_message = ' ' . $t('(Only required when enabling encryption settings)');
        $warning_message .= $encryption_use_message;
        $requirements[$extension . '_extension']['description'] .= $encryption_use_message;
      }
      drupal_set_message(check_plain($warning_message), 'warning');
    }
    else {
      $requirements[$extension . '_extension']['severity'] = REQUIREMENT_OK;
    }
  }
}

/**
 * Create serparated table to store webform last submission id to sFTP.
 */
function webform2sftp_update_7001() {
  if (db_field_exists('webform', 'webform2sftp_last_webform_submission_sid')) {
    $results = webform2sftp_get_webforms_last_submission_to_sftp();

    if (!db_table_exists('webform2sftp_last_submission_id_to_sftp')) {
      db_create_table('webform2sftp_last_submission_id_to_sftp', drupal_get_schema('webform2sftp_last_submission_id_to_sftp', TRUE));
    }

    // Migrate last submission ids to new table created.
    webform2sftp_insert_last_submission_id_sent($results);

    db_drop_field('webform', 'webform2sftp_last_webform_submission_sid');
  }
}

/**
 * Get webform last submission ids to sFTP from "webform" table.
 */
function webform2sftp_get_webforms_last_submission_to_sftp() {
  $query = db_select('webform', 'wf');
  $query->fields('wf', array('nid', 'webform2sftp_last_webform_submission_sid'));

  $query_results = $query->execute();

  $results = $query_results->fetchAll();

  return $results;
}

/**
 * Insert last submission ids from "webform" table to new table created.
 */
function webform2sftp_insert_last_submission_id_sent($results) {
  foreach ($results as $webform) {
    // Set default value for empty values.
    if (empty($webform->webform2sftp_last_webform_submission_sid)) {
      $webform->webform2sftp_last_webform_submission_sid = -1;
    }

    db_insert('webform2sftp_last_submission_id_to_sftp')
      ->fields(array(
        'nid' => $webform->nid,
        'webform2sftp_last_webform_submission_sid' => $webform->webform2sftp_last_webform_submission_sid,
      ))
      ->execute();
  }
}
