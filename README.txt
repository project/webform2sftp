CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Terminology
 * Instalation
 * Configuration


INTRODUCTION
------------
This module provides the functionality to export webform submissions to a
 SFTP server.


TERMINOLOGY
-----------
Useful to export form data to a data file being sent to a sFTP server which
would be imported and consumed by a third system.

* Security:
The module focuses on the integrity of the data to be exported. The data is sent
 via secure connection (sFTP) and it's possible to encrypt (PGP) the data
file before sending it.

* Multiple Integration
The module serves multiple webforms by separating the settings for each one.

* File data
It's a file containing a submission in each line, separating the field
values with a character of your choice, usually pipe "|".

* Schedule and automation
You can automate and schedule data exports, the Job will be executed by system
cron in the time you set.

* Notification
You can configure an email to be sent when the Job executes successfully.

* Encryption
It's possible to use PGP encryption in the data file before it is sent to sFTP.


INSTALATION
-----------
- IMPORTANT: The module uses the PHP extension SSH2 to connect to SFTP servers
and GnuPG extension for file PGP encryption, so installing these extensions are
REQUIRED for the module works, otherwise errors will occur.

gnupg
# Debian systems:
sudo apt-get update && sudo apt-get install php-pear php5-dev libgpgme11-dev
sudo pecl install gnupg

# you may need to create /etc/php5/apache2/conf.d/gnupg.ini file with the
# line below:
extension=gnupg.so

libssh-2
# On Ubuntu:
sudo apt-get install libssh2-1-dev libssh2-php

# Make sure to restart Apache after installing the extensions:
sudo service apache2 restart

- After installed the required extensions, install the module in the normal
Drupal way. Access admin/modules and select webform2sftp.


CONFIGURATION
-------------
- Module global settings, like SFTP Keys, Schedule, notifications, access:
admin/config/content/webform

- Each webform have your own settings, access:
node/%webform/webform/sftp-export
