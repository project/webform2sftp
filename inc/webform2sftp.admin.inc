<?php

/**
 * @file
 * Webform2sftp admin settings.
 */

/**
 * Form alter constructor to global settings.
 */
function webform2sftp_global_settings_form(&$form) {
  module_load_include('inc', 'webform2sftp', 'inc/webform2sftp.common');

  $form['webform2sftp_global_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Export to SFTP'),
    '#description' => t('Settings to allow the system to export webform submissions to an SFTP server'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );

  $form['webform2sftp_global_settings']['sftp'] = array(
    '#type' => 'fieldset',
    '#title' => t('SFTP Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  $form['webform2sftp_global_settings']['sftp']['address'] = array(
    '#type' => 'textfield',
    '#title' => t('SFTP Address'),
    '#description' => t('The SFTP server address that will receive the files. Do not include the protocol in the address.'),
    '#default_value' => webform2sftp_get_settings('sftp', 'address'),
    '#required' => TRUE,
  );

  $form['webform2sftp_global_settings']['sftp']['repository'] = array(
    '#type' => 'textfield',
    '#title' => t('Files destination repository'),
    '#description' => t('Folder in SFTP server where the files will be transferred. E.g.: /home/repo/inbound.'),
    '#default_value' => webform2sftp_get_settings('sftp', 'repository'),
    '#required' => TRUE,
  );

  $form['webform2sftp_global_settings']['sftp']['user'] = array(
    '#type' => 'textfield',
    '#title' => t('SFTP Username'),
    '#description' => t('Server access user name.'),
    '#default_value' => webform2sftp_get_settings('sftp', 'user'),
    '#required' => TRUE,
  );

  $form['webform2sftp_global_settings']['sftp']['password'] = array(
    '#type' => 'password',
    '#title' => t('SFTP Password'),
    '#description' => t('Server access Password.'),
    '#attributes' => array('value' => webform2sftp_get_settings('sftp', 'password')),
    '#required' => TRUE,
  );

  // Use the #managed_file FAPI element to upload the keys.
  $form['webform2sftp_global_settings']['sftp']['public_key'] = array(
    '#title' => t('SFTP Public Key'),
    '#type' => 'managed_file',
    '#progress_message' => t('Please wait...'),
    '#progress_indicator' => 'bar',
    '#upload_validators' => array(
      'file_validate_extensions' => array('pub'),
    ),
    '#description' => t('Public key that will be uploaded in Drupal Private file system path. <br />
                         Notice: The public key MUST be in OpenSSH authorized_keys file format.'),
    '#default_value' => webform2sftp_get_settings('sftp', 'public_key'),
    '#upload_location' => 'private://',
  );

  $form['webform2sftp_global_settings']['sftp']['private_key'] = array(
    '#title' => t('SFTP Private Key'),
    '#type' => 'managed_file',
    '#progress_message' => t('Please wait...'),
    '#progress_indicator' => 'bar',
    '#upload_validators' => array(
      'file_validate_extensions' => array('ppk'),
    ),
    '#description' => t('PPK Key that will be uploaded in Drupal Private file system path. <br />
                         Notice: The public key MUST be in OpenSSH format.'),
    '#default_value' => webform2sftp_get_settings('sftp', 'private_key'),
    '#upload_location' => 'private://',
  );

  $form['webform2sftp_global_settings']['encryption'] = array(
    '#type' => 'fieldset',
    '#title' => t('Encryption'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  $form['webform2sftp_global_settings']['encryption']['key'] = array(
    '#title' => t('PGP public key'),
    '#type' => 'managed_file',
    '#progress_message' => t('Please wait...'),
    '#progress_indicator' => 'bar',
    '#upload_validators' => array(
      'file_validate_extensions' => array('asc', 'ppk', 'pub', 'txt', 'key'),
    ),
    '#description' => t('PGP public Key that will be uploaded in Drupal Private file system path. <br />
                         This key is used to encrypt the file to be sent to sFTP if the flag is enabled on webform node settings.'),
    '#default_value' => webform2sftp_get_settings('encryption', 'key'),
    '#upload_location' => 'private://',
  );

  $form['webform2sftp_global_settings']['integration'] = array(
    '#type' => 'fieldset',
    '#title' => t('Integration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  $form['webform2sftp_global_settings']['integration']['delimiter_character'] = array(
    '#type' => 'textfield',
    '#title' => t('Delimiter character'),
    '#description' => t('Delimiter character used to export Webform results.'),
    '#default_value' => webform2sftp_get_delimiter_character(),
    '#size' => 1,
    '#maxlength' => 1,
    '#required' => TRUE,
  );

  $form['webform2sftp_global_settings']['integration']['line_ending'] = array(
    '#type' => 'radios',
    '#title' => t('Line Ending Character'),
    '#description' => t('Type of line endings to use in the generated file.'),
    '#default_value' => webform2sftp_get_line_ending_value(),
    '#options' => array(0 => t('LF'), 1 => ('CRLF')),
    '#required' => TRUE,
  );

  $form['webform2sftp_global_settings']['integration']['enabled'] = array(
    '#type' => 'checkbox',
    '#prefix' => '<strong>' . t('Integration with SFTP') . '</strong>',
    '#title' => t('Enable Integration to generate the files and submit through the SFTP channel.'),
    '#default_value' => webform2sftp_is_global_integration_enabled(),
  );

  $form['webform2sftp_global_settings']['integration']['decrypt_data'] = array(
    '#type' => 'checkbox',
    '#prefix' => '<strong>' . t('Decrypt Submissions Data') . '</strong>',
    '#title' => t('Decrypt submission data before generating file?'),
    '#default_value' => webform2sftp_is_to_decrypt_webform_data(),
    '#description' => t('If using webform data encryption methods (such as Webform Encrypt), the data will be decrypted before the files are generated.
                         Note that this will not affect PGP encryption.'),
  );

  $form['webform2sftp_global_settings']['integration']['trigger_file'] = array(
    '#type' => 'checkbox',
    '#prefix' => '<strong>' . t('Trigger File') . '</strong>',
    '#title' => t('Generate trigger file?'),
    '#default_value' => webform2sftp_is_to_generate_trigger_file(),
    '#description' => t('An additional file will be generated and sent to SFTP. The file contains number of records and number of lines.'),
  );

  $form['webform2sftp_global_settings']['integration']['cleanup_database'] = array(
    '#type' => 'checkbox',
    '#prefix' => '<strong>' . t('Database entries Cleanup') . '</strong>',
    '#title' => t('Delete entries from database after sending files to SFTP.'),
    '#default_value' => webform2sftp_get_settings('integration', 'cleanup_database'),
  );

  $email_settings = webform2sftp_get_settings('integration', 'email');
  $form['webform2sftp_global_settings']['integration']['email'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email'),
    '#description' => t('Allows system to send an email after completing the Job.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );

  $form['webform2sftp_global_settings']['integration']['email']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send a Email after completing the Job?'),
    '#default_value' => webform2sftp_is_to_send_email(),
    '#required' => FALSE,
  );

  $form['webform2sftp_global_settings']['integration']['email']['from'] = array(
    '#type' => 'textfield',
    '#title' => t('Email From'),
    '#required' => FALSE,
    '#default_value' => isset($email_settings['from']) ? $email_settings['from'] : NULL,
  );

  $form['webform2sftp_global_settings']['integration']['email']['to'] = array(
    '#type' => 'textfield',
    '#title' => t('Email To'),
    '#required' => FALSE,
    '#default_value' => isset($email_settings['to']) ? $email_settings['to'] : NULL,
  );

  $form['webform2sftp_global_settings']['integration']['email']['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#required' => FALSE,
    '#default_value' => isset($email_settings['subject']) ? $email_settings['subject'] : NULL,
  );

  $form['webform2sftp_global_settings']['integration']['email']['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#required' => FALSE,
    '#default_value' => isset($email_settings['message']) ? $email_settings['message'] : NULL,
    '#description' => t('You can use the tokens: [files], [number_of_records] and [date]'),
  );

  $form['webform2sftp_global_settings']['integration']['schedule'] = array(
    '#type' => 'fieldset',
    '#title' => t('Schedule'),
    '#description' => t('Configure date and time in which files can be exported and sent through SFTP during system cron execution.<br />
                        You must configure the CRONTAB on the Web Server to call the cron.php.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  $schedule_settings = webform2sftp_get_settings('integration', 'schedule');
  $form['webform2sftp_global_settings']['integration']['schedule']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Job Schedule?'),
    '#default_value' => isset($schedule_settings['enabled']) ? $schedule_settings['enabled'] : NULL,
    '#required' => FALSE,
  );

  $form['webform2sftp_global_settings']['integration']['schedule']['week'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Day of the week'),
    '#options' => array(
      'Sun' => t('Sunday'),
      'Mon' => t('Monday'),
      'Tue' => t('Tuesday'),
      'Wed' => t('Wednesday'),
      'Thu' => t('Thursday'),
      'Fri' => t('Friday'),
      'Sat' => t('Saturday'),
    ),
    '#description' => t('If no value is entered and the schedule is enabled, the process will be executed every day.'),
    '#default_value' => webform2sftp_get_schedule_week_days(),
    '#required' => FALSE,
  );

  $form['webform2sftp_global_settings']['integration']['schedule']['hour'] = array(
    '#type' => 'select',
    '#title' => t('Time'),
    '#description' => t('If no value is entered and the schedule is enabled, the process will be executed at 00h'),
    '#options' => webform2sftp_get_batch_run_time_options(),
    '#default_value' => webform2sftp_get_schedule_hour(),
    '#required' => FALSE,
  );

  $form['webform2sftp_global_settings']['integration']['schedule']['timezone'] = array(
    '#type' => 'select',
    '#title' => t('Timezone'),
    '#description' => t('Select the schedule timezone.'),
    '#options' => system_time_zones(),
    '#default_value' => webform2sftp_get_schedule_timezone(),
    '#required' => FALSE,
  );

  array_unshift($form['#submit'], 'webform2sftp_global_settings_form_handler_files_submit');
}

/**
 * Returns the options time to use to scheduling the cron job execution time.
 */
function webform2sftp_get_batch_run_time_options() {
  $hours = array();

  for ($hour = 0; $hour <= 23; $hour++) {
    $formatted_hour = str_pad($hour, 2, '0', STR_PAD_LEFT);
    $hours[$formatted_hour] = $formatted_hour . ' hr';
  }

  return $hours;
}

/**
 * Admin form to configure Development mode.
 */
function webform2sftp_development_form() {
  $form['webform2sftp_development_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Development Mode'),
    '#description' => t('Enable Development Mode to bypass cron validations. Always perform webform2sftp job on cron run.'),
    '#default_value' => variable_get('webform2sftp_development_mode', ''),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}
