<?php

/**
 * @file
 * Webform2sftp global definition.
 */

/**
 * Indicates that a file is permanent and should not be deleted.
 *
 * @param int $file_id
 *   The file id.
 */
function webform2sftp_set_file_status_to_permanent($file_id) {
  if (!empty($file_id)) {
    $file_object = file_load($file_id);
    $file_object->status = FILE_STATUS_PERMANENT;
    file_save($file_object);
    file_usage_add($file_object, 'webform2sftp', 'webform2sftp', $file_object->fid);
  }
}

/**
 * Returns the module global settings stored in a variable.
 */
function webform2sftp_get_global_settings() {
  return variable_get('webform2sftp_global_settings', array());
}

/**
 * Returns a specific item from settings.
 *
 * @param string $section
 *   The settings section.
 * @param string $item
 *   The item inside a section.
 *
 * @return array
 *   The specific settings if exist, NULL otherwise.
 */
function webform2sftp_get_settings($section, $item) {
  $global_settings = webform2sftp_get_global_settings();

  return isset($global_settings[$section][$item]) ? $global_settings[$section][$item] : NULL;
}

/**
 * Checks if the integration is enabled to perform the Jobs.
 */
function webform2sftp_is_global_integration_enabled() {
  $integration_enabled = webform2sftp_get_settings('integration', 'enabled');

  return !empty($integration_enabled) ? TRUE : FALSE;
}

/**
 * Checks if the submissions data should be decrypted before generating files.
 */
function webform2sftp_is_to_decrypt_webform_data() {
  $decrypt_data = webform2sftp_get_settings('integration', 'decrypt_data');

  return !empty($decrypt_data) ? TRUE : FALSE;
}

/**
 * Checks if the cleanup database flag is enabled.
 */
function webform2sftp_is_cleanup_database_enabled() {
  $cleanup_enabled = webform2sftp_get_settings('integration', 'cleanup_database');

  return !empty($cleanup_enabled) ? TRUE : FALSE;
}

/**
 * Checks if trigger file should be generated.
 */
function webform2sftp_is_to_generate_trigger_file() {
  $trigger_file_enabled = webform2sftp_get_settings('integration', 'trigger_file');

  return !empty($trigger_file_enabled) ? TRUE : FALSE;
}

/**
 * Checks if the schedule is enabled to perform the Jobs.
 */
function webform2sftp_is_schedule_enabled() {
  $schedule_settings = webform2sftp_get_settings('integration', 'schedule');

  return !empty($schedule_settings['enabled']) ? TRUE : FALSE;
}

/**
 * Returns the timezone from schedule.
 */
function webform2sftp_get_schedule_timezone() {
  $schedule_settings = webform2sftp_get_settings('integration', 'schedule');

  return !empty($schedule_settings['timezone']) ? $schedule_settings['timezone'] : date_default_timezone_get();
}

/**
 * Returns the day of the week set on schedule configuration.
 */
function webform2sftp_get_schedule_week_days() {
  $schedule_settings = webform2sftp_get_settings('integration', 'schedule');

  $default_days = array(
    'Sun',
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat',
  );

  return !empty($schedule_settings['week']) ? $schedule_settings['week'] : $default_days;
}

/**
 * Returns the hour set on schedule configuration.
 */
function webform2sftp_get_schedule_hour() {
  $schedule_settings = webform2sftp_get_settings('integration', 'schedule');

  return !empty($schedule_settings['hour']) ? $schedule_settings['hour'] : '00';
}

/**
 * Checks if current time/date is in programmed schedule hour.
 */
function webform2sftp_is_in_schedule_period_hour() {
  $is_in_period = FALSE;

  $schedule_timezone = webform2sftp_get_schedule_timezone();
  $current_date = new DateTime('now', new DateTimeZone($schedule_timezone));

  $schedule_hour = webform2sftp_get_schedule_hour();
  if ($current_date->format('H') == $schedule_hour) {
    $is_in_period = TRUE;
  }

  return $is_in_period;
}

/**
 * Checks if current time/date is in programmed schedule week day.
 */
function webform2sftp_is_in_schedule_period_week_day() {
  $is_in_schedule_week_day = FALSE;

  $schedule_timezone = webform2sftp_get_schedule_timezone();
  $current_date = new DateTime('now', new DateTimeZone($schedule_timezone));

  $schedule_week_days = webform2sftp_get_schedule_week_days();
  if (!empty($schedule_week_days[$current_date->format('D')])) {
    $is_in_schedule_week_day = TRUE;
  }

  return $is_in_schedule_week_day;
}

/**
 * Checks if current time/date is in programmed schedule.
 */
function webform2sftp_is_in_schedule_period() {
  $is_in_schedule_period = FALSE;

  if (webform2sftp_is_in_schedule_period_hour() && webform2sftp_is_in_schedule_period_week_day()) {
    $is_in_schedule_period = TRUE;
  }

  return $is_in_schedule_period;
}

/**
 * Verifies if cron should perform job checking the mandatory checks.
 */
function webform2sftp_cron_should_perform_job() {
  $should_perform_job = FALSE;

  if ((webform2sftp_is_global_integration_enabled() && webform2sftp_is_schedule_enabled() && webform2sftp_is_in_schedule_period() && !webform2sftp_job_already_executed_today()) || webform2sftp_development_mode_enabled()) {
    $should_perform_job = TRUE;
  }

  return $should_perform_job;
}

/**
 * Defines a weight to a webform component.
 *
 * @param int $component
 *   The webform component ID.
 * @param int $weight
 *   The weight of the webform component.
 */
function webform2sftp_insert_entry_components_order($component, $weight) {
  db_insert('webform2sftp_components_order')
    ->fields(array(
      'nid' => $component['nid'],
      'cid' => $component['cid'],
      'disabled' => 0,
      'weight' => $weight,
    ))
    ->execute();
}

/**
 * Removes a webform component order from database.
 *
 * @param int $cid
 *   The webform component ID.
 * @param int $nid
 *   The given node id.
 */
function webform2sftp_delete_entry_components_order($cid, $nid) {
  $success = db_delete('webform2sftp_components_order')
    ->condition('cid', $cid)
    ->condition('nid', $nid)
    ->execute();

  if ($success) {
    drupal_set_message(t('The component was successfully deleted and will not be exported to sFTP.'));
  }
}

/**
 * Updates a webform component order entry.
 *
 * @param int $cid
 *   The webform component ID.
 * @param int $nid
 *   The node id.
 * @param array $form_state
 *   Form_state array format.
 */
function webform2sftp_update_entry_components_order($cid, $nid, array $form_state) {
  db_update('webform2sftp_components_order')
    ->fields(array(
      'weight' => $form_state['input']['weight' . $cid],
      'disabled' => isset($form_state['input']['disabled' . $cid]) ? $form_state['input']['disabled' . $cid] : 0,
    ))
    ->condition('cid', $cid, '=')
    ->condition('nid', $nid, '=')
    ->execute();
}

/**
 * Deletes record entries of components order by node.
 *
 * @param int $nid
 *   The node id.
 */
function webform2sftp_delete_components_order_by_node($nid) {
  db_delete('webform2sftp_components_order')
    ->condition('nid', $nid)
    ->execute();
}

/**
 * Deletes webform submissions.
 *
 * @param int $first_sid
 *   The first webform submission id.
 * @param int $last_sid
 *   The last webform submission id.
 * @param int $nid
 *   The webform node id.
 */
function webform2sftp_delete_submissions($first_sid, $last_sid, $nid) {
  $tables = array(
    'webform_submitted_data',
    'webform_submissions',
  );

  foreach ($tables as $table) {
    db_delete($table)
      ->condition('sid', $first_sid, '>=')
      ->condition('sid', $last_sid, '<=')
      ->condition('nid', $nid)
      ->execute();
  }

  watchdog('webform2sftp', 'The webform submissions were deleted.');
}

/**
 * Returns all webforms that are set to be integrated with sftp.
 */
function webform2sftp_get_integrated_webforms() {
  $webforms_integrated = array();

  $webforms_integrated_query = db_select('webform')
    ->fields('webform', array('nid'))
    ->condition('webform2sftp_integration_flag', '1', '=')
    ->execute();

  while ($record = $webforms_integrated_query->fetchAssoc()) {
    $webforms_integrated[] = $record['nid'];
  }

  return $webforms_integrated;
}

/**
 * Verify if a webform node requires PGP encryption to export file.
 *
 * @param int $nid
 *   The webform nid.
 *
 * @return bool
 *   TRUE if webform export file requires a PGP encryption, FALSE otherwise.
 */
function webform2sftp_is_pgp_encryption_enabled($nid) {
  $is_pgp_encryption_enabled = FALSE;

  $pgp_encryption_flag = db_select('webform')
    ->fields('webform', array('webform2sftp_encryption_flag'))
    ->condition('nid', $nid, '=')
    ->execute()
    ->fetchAssoc();

  if (!empty($pgp_encryption_flag['webform2sftp_encryption_flag'])) {
    $is_pgp_encryption_enabled = TRUE;
  }

  return $is_pgp_encryption_enabled;
}

/**
 * Returns the file delimiter character.
 */
function webform2sftp_get_delimiter_character() {
  $delimiter = webform2sftp_get_settings('integration', 'delimiter_character');
  return $delimiter ? $delimiter : '|';
}

/**
 * Returns the file line ending form value.
 */
function webform2sftp_get_line_ending_value() {
  $value = webform2sftp_get_settings('integration', 'line_ending');
  return $value ? $value : 0;
}

/**
 * Returns the file line ending.
 */
function webform2sftp_get_line_ending() {
  $value = webform2sftp_get_line_ending_value();

  if ($value == 0) {
    $delimiter = "\n";
  }
  else {
    $delimiter = "\r\n";
  }

  return $delimiter;
}

/**
 * Checks if is to module sent a email on end of Job.
 */
function webform2sftp_is_to_send_email() {
  $email_settings = webform2sftp_get_settings('integration', 'email');
  return isset($email_settings['enabled']) ? $email_settings['enabled'] : FALSE;
}

/**
 * Set the last webform submission ID by a given webform node ID.
 *
 * @param int $last_submission_sid
 *   The last submission sid sent successfully to sftp.
 * @param int $nid
 *   The webform node id.
 */
function webform2sftp_update_last_submission_sid($last_submission_sid, $nid) {
  db_update('webform2sftp_last_submission_id_to_sftp')
    ->fields(array('webform2sftp_last_webform_submission_sid' => $last_submission_sid))
    ->condition('nid', $nid, '=')
    ->execute();
}

/**
 * Returns the last submission sid from an array of submissions.
 *
 * @param array $submissions
 *   Webform submissions from a specific webform.
 *
 * @return int
 *   The last submission sid.
 */
function webform2sftp_get_last_submission_id(array $submissions) {
  end($submissions);
  return key($submissions);
}

/**
 * Returns the first submission sid from an array of submissions.
 *
 * @param array $submissions
 *   Webform submissions from a specific webform.
 *
 * @return int
 *   The last submission sid.
 */
function webform2sftp_get_first_submission_id(array $submissions) {
  reset($submissions);
  return key($submissions);
}

/**
 * Retrieves all webforms from database.
 */
function webform2sftp_get_webforms() {
  $query = db_query("SELECT n.nid FROM {node} n WHERE n.type = 'webform'");
  $result = $query->fetchAll();

  return $result;
}

/**
 * Get sFTP options from webform admin configuration.
 *
 * @return array
 *   An array of SFTP options containing the keys:
 *     array(
 *       'address',
 *       'user',
 *       'pass',
 *       'public_key',
 *       'private_key',
 *     );
 */
function webform2sftp_get_sftp_options() {
  return array(
    'address' => webform2sftp_get_settings('sftp', 'address'),
    'user' => webform2sftp_get_settings('sftp', 'user'),
    'pass' => webform2sftp_get_settings('sftp', 'password'),
    'public_key' => webform2sftp_get_realpath_by_fid(webform2sftp_get_settings('sftp', 'public_key')),
    'private_key' => webform2sftp_get_realpath_by_fid(webform2sftp_get_settings('sftp', 'private_key')),
  );
}

/**
 * Returns the file path from a given file id.
 */
function webform2sftp_get_realpath_by_fid($fid) {
  $file_path = '';

  if (!is_null($fid)) {
    $file_object = file_load($fid);

    if ($file_object) {
      $file_path = drupal_realpath($file_object->uri);
    }
  }

  return $file_path;
}

/**
 * Send a email notification.
 */
function webform2sftp_send_email_notification($tokens) {
  $token_files = '';
  foreach ($tokens['[files]'] as $file_path) {
    $token_files .= basename($file_path) . "\r\n";
  }
  $tokens['[files]'] = $token_files;

  $email_settings = webform2sftp_get_settings('integration', 'email');
  global $language;

  drupal_mail('webform2sftp', 'notification', $email_settings['to'], $language, array('tokens' => $tokens), $email_settings['from']);
}

/**
 * Replaces the tokens with the related values in the given e-mail message.
 */
function webform2sftp_replace_email_tokens($message, $email_tokens) {
  foreach ($email_tokens as $token => $token_value) {
    $message = str_replace($token, $token_value, $message);
  }

  return $message;
}

/**
 * Checks if a scheduled Job was already executed today.
 */
function webform2sftp_job_already_executed_today() {
  $latest_job_executed = variable_get('webform2sftp_last_cron_run', date('Ymd', strtotime("yesterday")));

  return $latest_job_executed == date('Ymd');
}

/**
 * Checks if Development mode is enabled.
 */
function webform2sftp_development_mode_enabled() {
  return variable_get('webform2sftp_development_mode', FALSE);
}
