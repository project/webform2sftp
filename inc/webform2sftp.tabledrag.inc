<?php

/**
 * @file
 * Functions for configuring components order for each webform using tabledrag .
 */

/**
 * Fill values of a row for the settings form.
 *
 * @param object $component
 *   Array containing all component information.
 *
 * @return array
 *   Array with new form row information.
 */
function _webform2sftp_get_filled_row($component) {
  $row = array(
    'disabled' => array(
      '#name' => 'disabled' . $component['cid'],
      '#type' => 'checkbox',
      '#checked' => isset($component['disabled']) ? $component['disabled'] : FALSE,
    ),
    'name' => array(
      '#name' => 'label' . $component['cid'],
      '#markup' => t('@label', array('@label' => $component['label'])),
    ),
    'weight' => array(
      '#name' => 'weight' . $component['cid'],
      '#type' => 'textfield',
      '#size' => 20,
      '#value' => isset($component['weight']) ? $component['weight'] : 1,
      '#attributes' => array('class' => array('components-weight')),
    ),
  );

  return $row;
}

/**
 * Return rendered HTML of $row.
 *
 * @param array $row
 *   Array with form api definition of all fields inside the $row.
 *
 * @return array
 *   Array with fields already rendered.
 */
function _webform2sftp_get_rendered_row_fields(array $row) {
  foreach ($row as $field => $row_data) {
    drupal_render($row_data);
    $row[$field] = $row_data['#children'];
  }

  return $row;
}

/**
 * Returns the rows of components table.
 */
function _webform2sftp_get_form_table_rows($node) {
  $components = _webform2sftp_get_components($node);

  $rows = array();
  foreach ($components as $component) {
    $rows[] = _webform2sftp_get_row_definition_from_component($component);
  }

  return $rows;
}

/**
 * Return an associative array containing the minimum row definition.
 *
 * Required by theme_table.
 *
 * @param object $component
 *   The object of a component.
 *
 * @return array
 *   An array containing the row definition.
 */
function _webform2sftp_get_row_definition_from_component($component) {
  $row = _webform2sftp_get_filled_row($component);
  $row = _webform2sftp_get_rendered_row_fields($row);

  $row_definition = array(
    'data' => $row,
    'class' => array('draggable'),
  );

  return $row_definition;
}
