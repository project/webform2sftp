<?php

/**
 * @file
 * Functions for configuring and editing sftp settings for each webform.
 */

/**
 * SFTP main configuration form for a webform node.
 */
function webform2sftp_configure_form($form, &$form_state, $node) {
  $form['#attached']['library'][] = array(
    'webform',
    'admin',
  );

  $form['#node'] = $node;

  $form['#submit'] = array('webform2sftp_configure_form_submit');

  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );
  $form['integration_flag'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable sFTP Export Integration'),
    '#description' => t('If this field is checked then sFTP module will export these webform submissions.'),
    '#default_value' => $node->webform['webform2sftp_integration_flag'],
  );

  $form['file'] = array(
    '#type' => 'fieldset',
    '#title' => t('File settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => FALSE,
  );

  $form['file']['file_name_prefix'] = array(
    '#type' => 'textfield',
    '#field_suffix' => '.yymmddhhmmss',
    '#title' => t('File Name Prefix'),
    '#description' => t('The file name prefix that will be sent to sFTP. The module appends date to the file name.'),
    '#default_value' => $node->webform['webform2sftp_file_name_prefix'],
  );

  module_load_include('inc', 'webform2sftp', 'inc/webform2sftp.tabledrag');
  drupal_add_tabledrag('components-table', 'order', 'sibling', 'components-weight');

  $header = array(
    'disabled' => t('Disabled'),
    'name' => t('Component Label'),
    'weight' => t('Weight'),
  );

  $form['file']['table_title'] = array('#markup' => '<strong>' . t('Components Order') . '</strong><br/>' . t('The order that components will be exported in file to sftp'));

  $form['file']['components_table'] = array(
    '#tree' => TRUE,
    '#header' => $header,
    '#rows' => _webform2sftp_get_form_table_rows($node),
    '#theme' => 'table',
    '#attributes' => array('id' => 'components-table'),
    '#empty'  => t('No content available.'),
  );

  $form['submissions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Submission settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => FALSE,
  );

  module_load_include('inc', 'webform2sftp', 'inc/webform2sftp.file');
  $webform2sftp_last_submission_sent = webform2sftp_get_last_submission_to_sftp($node->nid);
  $form['submissions']['last_webform_submission_sid'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Webform Submission Id'),
    '#description' => t('The sid of the last submission data transferred successfully to sFTP. Leave -1 to reset.'),
    '#default_value' => $webform2sftp_last_submission_sent,
  );

  $form['submissions']['submission_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Submission Limit'),
    '#description' => t('The limit of submissions per Job. Leave blank to get all submissions.'),
    '#default_value' => !empty($node->webform['webform2sftp_submission_limit']) ? $node->webform['webform2sftp_submission_limit'] : NULL,
  );

  $form['encryption'] = array(
    '#type' => 'fieldset',
    '#title' => t('Encryption settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => FALSE,
  );

  $form['encryption']['encryption_flag'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable PGP Encryption'),
    '#description' => t('If this field is checked then file generated will encrypted.'),
    '#default_value' => $node->webform['webform2sftp_encryption_flag'],
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  $form['actions']['run_now'] = array(
    '#type' => 'button',
    '#value' => t('Export Now'),
    '#name' => 'export_now',
    '#executes_submit_callback' => TRUE,
  );

  return $form;
}

/**
 * Submit handler for webform2sftp_configure_form().
 */
function webform2sftp_configure_form_submit($form, &$form_state) {
  $node = &$form['#node'];
  if ($form_state['clicked_button']['#name'] == 'export_now') {
    module_load_include('inc', 'webform2sftp', 'inc/webform2sftp.export');
    if (webform2sftp_job_export_webform_submissions_to_sftp($node->nid)) {
      drupal_set_message(t('The submissions were sent successfully.'));
    }
    else {
      drupal_set_message(t('Unable to process the export. Please, check the Log to get informations.'), 'error');
    }
  }
  else {
    $node->webform['webform2sftp_integration_flag'] = $form_state['values']['integration_flag'];
    $node->webform['webform2sftp_encryption_flag'] = $form_state['values']['encryption_flag'];
    $node->webform['webform2sftp_submission_limit'] = $form_state['values']['submission_limit'];
    $node->webform['webform2sftp_file_name_prefix'] = $form_state['values']['file_name_prefix'];
    node_save($node);

    module_load_include('inc', 'webform2sftp', 'inc/webform2sftp.common');
    webform2sftp_update_last_submission_sid($form_state['values']['last_webform_submission_sid'], $node->nid);

    _webform2sftp_save_components_order($form['#node'], $form_state);
    drupal_set_message(t('The sFTP settings have been updated.'));
  }
}

/**
 * Save components that should be exported and respective order.
 *
 * @param object $node
 *   The webform node object.
 * @param array $form_state
 *   The values submitted in sFTP admin form.
 */
function _webform2sftp_save_components_order($node, array $form_state) {
  module_load_include('inc', 'webform2sftp', 'inc/webform2sftp.common');

  $components = _webform2sftp_get_components($node);
  foreach ($components as $component) {
    webform2sftp_update_entry_components_order($component['cid'], $node->nid, $form_state);
  }
}

/**
 * Returns the components of a given webform.
 *
 * @param object $node
 *   The webform node object.
 *
 * @return array
 *   An array containing components cid, weight and label from $node.
 */
function _webform2sftp_get_components($node) {
  $components_order_query = db_select('webform2sftp_components_order', 'co')
    ->fields('co', array('id', 'nid', 'cid', 'disabled', 'weight'))
    ->condition('nid', $node->nid, '=')
    ->orderBy('weight')
    ->execute();

  $components_order = array();
  while ($component_order = $components_order_query->fetchAssoc()) {
    $components_order[$component_order['id']]['cid'] = $component_order['cid'];
    $components_order[$component_order['id']]['weight'] = $component_order['weight'];
    $components_order[$component_order['id']]['disabled'] = $component_order['disabled'];

    $component_label = db_select('webform_component', 'wc')
      ->fields('wc', array('name'))
      ->condition('cid', $component_order['cid'], '=')
      ->condition('nid', $component_order['nid'], '=')
      ->execute()
      ->fetchAssoc();
    $components_order[$component_order['id']]['label'] = $component_label['name'];
  }

  return $components_order;
}
