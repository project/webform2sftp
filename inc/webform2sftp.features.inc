<?php

/**
 * @file
 * Features integration hooks.
 */

/**
 * Implements hook_features_export_options().
 */
function webform2sftp_components_config_features_export_options() {
  $options = array();
  $query = db_select('webform_component', 'wfc');
  $query->fields('wfc', array('nid', 'cid', 'name'));

  $query_results = $query->execute();
  $results = $query_results->fetchAll();

  foreach ($results as $component) {
    $options["{$component->nid}_{$component->cid}"] = "nid-{$component->nid}: {$component->name}";
  }

  return $options;
}

/**
 * Implements hook_features_export().
 */
function webform2sftp_components_config_features_export($data, &$export, $module_name) {
  $export['dependencies']['webform2sftp'] = 'webform2sftp';

  foreach ($data as $key => $component) {
    $export['features']['webform2sftp_components_config'][$key] = $key;
  }

  return array();
}

/**
 * Implements hook_features_export_render().
 */
function webform2sftp_components_config_features_export_render($module_name, $data, $export = NULL) {
  $code = array();
  $code[] = '  $webform2sftp_components_config = array();';
  $code[] = '';
  foreach ($data as $component_key) {
    $item = webform2sftp_components_config_get_data($component_key);
    $code[] = '  $webform2sftp_components_config["' . $component_key . '"] = ' . features_var_export($item, '  ') . ';';
  }
  $code[] = '  return $webform2sftp_components_config;';
  $code = implode("\n", $code);

  return array('webform2sftp_components_config_features_default_settings' => $code);
}

/**
 * Get component data from database.
 */
function webform2sftp_components_config_get_data($component_key) {
  $component = webform2sftp_get_component_nid_cid($component_key);

  $query = db_select('webform2sftp_components_order', 'wfco');
  $query->fields('wfco', array('disabled', 'weight'));
  $query->condition('nid', $component['nid'], '=');
  $query->condition('cid', $component['cid'], '=');

  $query_results = $query->execute();
  $results = $query_results->fetchObject();

  return $results;
}

/**
 * Implements hook_features_rebuild().
 */
function webform2sftp_components_config_features_rebuild($module) {
  $items = module_invoke($module, 'webform2sftp_components_config_features_default_settings');

  foreach ($items as $key => $item) {
    webform2sftp_components_config_set_data($key, $item);
  }
}

/**
 * Set component data to database.
 */
function webform2sftp_components_config_set_data($component_key, $item) {
  $component = webform2sftp_get_component_nid_cid($component_key);

  db_update('webform2sftp_components_order')
    ->fields(array(
      'disabled' => $item['disabled'],
      'weight' => $item['weight'],
    ))
    ->condition('nid', $component['nid'], '=')
    ->condition('cid', $component['cid'], '=')
    ->execute();
}

/**
 * Get component nid and cid separately.
 */
function webform2sftp_get_component_nid_cid($component_key) {
  $component_keys = explode('_', $component_key);
  $component['nid'] = $component_keys[0];
  $component['cid'] = $component_keys[1];

  return $component;
}

/**
 * Implements hook_features_revert().
 */
function webform2sftp_components_config_features_revert($module) {
  webform2sftp_components_config_features_rebuild($module);
}
