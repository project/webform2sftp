<?php

/**
 * @file
 * Webform2sftp export webform data handler functions.
 */

/**
 * Encrypts an array of files using PGP method.
 *
 * @param string $files
 *   An array of files path to be encrypted.
 * @param string $pgp_public_key
 *   The path of the PGP public key.
 *
 * @return array
 *   An array of paths in case of success, empty array otherwise.
 */
function webform2sftp_encrypt_files($files, $pgp_public_key) {
  $encrypted_files = array();

  foreach ($files as $type => $file) {
    $encrypt_file_content = Webform2SftpPGP::encryptFileContent($file, $pgp_public_key);

    if ($encrypt_file_content) {
      $encrypted_file = file_save_data($encrypt_file_content, 'temporary://' . basename($file) . '.pgp', FILE_EXISTS_REPLACE);
    }

    if ($encrypted_file) {
      watchdog('webform2sftp', 'The encrypted file @file was successfully generated', array('@file' => basename($file)));
      $encrypted_files[$type] = drupal_realpath($encrypted_file->uri);
    }
    else {
      watchdog('webform2sftp', 'Unable to encrypt the file @file', array('@file' => basename($file)));
      $encrypted_files[$type] = NULL;
    }
  }

  return !(in_array(NULL, $encrypted_files)) ? $encrypted_files : array();
}

/**
 * Sends a list of files to a SFTP repository.
 *
 * @param array $files
 *   An array of files to be sent.
 *
 * @return bool
 *   TRUE in case of success, FALSE otherwise.
 */
function webform2sftp_send_files_to_sftp(array $files) {
  module_load_include('inc', 'webform2sftp', 'inc/webform2sftp.common');

  $sftp_repository = webform2sftp_get_settings('sftp', 'repository');
  $sftp_options = webform2sftp_get_sftp_options();
  $successful_post = array();

  if (in_array(NULL, array($sftp_repository) + $sftp_options, TRUE)) {
    watchdog('webform2sftp', 'Unable to export. The following options are required to process the file: @variables', array('@variables' => print_r($sftp_options, TRUE)));

    return FALSE;
  }

  $sftp = new Webform2SftpSSH2($sftp_options['address'], $sftp_options['user'], $sftp_options['pass'], $sftp_options['public_key'], $sftp_options['private_key']);
  foreach ($files as $type => $file) {
    if ($sftp->sftpSendFile($file, $sftp_repository)) {
      $successful_post[$type] = TRUE;
      watchdog('webform2sftp', 'The file @file was successfully uploaded to the SFTP server.', array('@file' => basename($file)));
    }
    else {
      $successful_post[$type] = FALSE;
      watchdog('webform2sftp', 'An error occurred while sending the file @file to the SFTP server.', array('@file' => basename($file)));
    }
  }

  return !in_array(FALSE, $successful_post);
}

/**
 * Job to performs webform submissions export to SFTP.
 */
function webform2sftp_process_export_sftp_jobs() {
  module_load_include('inc', 'webform2sftp', 'inc/webform2sftp.common');

  $webforms_integrated = webform2sftp_get_integrated_webforms();
  foreach ($webforms_integrated as $webform_nid) {
    webform2sftp_job_export_webform_submissions_to_sftp($webform_nid);
  }
}

/**
 * Process Job to export a given webform submissions to a sftp server.
 *
 * @param int $webform_nid
 *   The given webform node id.
 */
function webform2sftp_job_export_webform_submissions_to_sftp($webform_nid) {
  module_load_include('inc', 'webform2sftp', 'inc/webform2sftp.common');
  module_load_include('inc', 'webform2sftp', 'inc/webform2sftp.file');

  $node = node_load($webform_nid);
  $submissions = webform2sftp_get_submissions_to_export($node);
  $file_name_prefix = $node->webform['webform2sftp_file_name_prefix'];

  $file_paths = webform2sftp_generate_files($submissions, $file_name_prefix);

  if (webform2sftp_is_pgp_encryption_enabled($webform_nid)) {
    $pgp_public_key_file_id = webform2sftp_get_settings('encryption', 'key');
    $pgp_public_key = webform2sftp_get_realpath_by_fid($pgp_public_key_file_id);
    if ($pgp_public_key) {
      $file_paths = webform2sftp_encrypt_files($file_paths, $pgp_public_key);
    }
    else {
      watchdog('webform2sftp', 'PGP Public key not loaded on settings form.');
    }
  }
  if ($file_paths) {
    // Send files.
    if (webform2sftp_send_files_to_sftp($file_paths)) {
      if (!empty($submissions)) {
        $last_submission_sid = webform2sftp_get_last_submission_id($submissions);
        webform2sftp_update_last_submission_sid($last_submission_sid, $webform_nid);

        // Cleanup process.
        if (webform2sftp_is_cleanup_database_enabled()) {
          $first_submission_sid = webform2sftp_get_first_submission_id($submissions);
          webform2sftp_delete_submissions($first_submission_sid, $last_submission_sid, $webform_nid);
        }
      }

      // Sent email.
      if (webform2sftp_is_to_send_email()) {
        $tokens = array(
          '[files]' => $file_paths,
          '[number_of_records]' => count($submissions),
          '[date]' => date('D M j H:i:s T Y'),
        );
        webform2sftp_send_email_notification($tokens);
      }

      return TRUE;
    }
  }
  else {
    watchdog('webform2sftp', 'Fail to generate file(s) of webform !webform_nid', array('!webform_nid' => $webform_nid));
  }

  return FALSE;
}
