<?php

/**
 * @file
 * Functions for generate files.
 */

/**
 * Returns webform submissions data from a specific node.
 *
 * @param object $node
 *   The node object.
 *
 * @return array
 *   NULL, if the webform node does not have new submissions to export
 *   Array of submissions, otherwise.
 */
function webform2sftp_get_submissions_to_export($node) {
  $last_submission_exported = webform2sftp_get_last_submission_to_sftp($node->nid);
  $submission_limit = $node->webform['webform2sftp_submission_limit'];

  $webform_submissions_query = db_select('webform_submissions')
    ->fields('webform_submissions', array('sid'))
    ->condition('nid', $node->nid, '=')
    ->condition('sid', $last_submission_exported->webform2sftp_last_webform_submission_sid, '>');

  if ($submission_limit) {
    $webform_submissions_query->range(0, $submission_limit);
  }
  $results = $webform_submissions_query->execute();

  $submissions_data = array();

  while ($submission = $results->fetchAssoc()) {
    $submissions_data[$submission['sid']] = _webform2sftp_get_submission_data_to_export($submission['sid']);
  }

  return $submissions_data;
}

/**
 * Get last submission id to sFTP for specified form.
 */
function webform2sftp_get_last_submission_to_sftp($nid) {
  $query = db_select('webform2sftp_last_submission_id_to_sftp', 'wls');
  $query->fields('wls', array('webform2sftp_last_webform_submission_sid'));
  $query->condition('nid', $nid, '=');

  $query_results = $query->execute();
  $results = $query_results->fetchObject();

  return $results;
}

/**
 * Returns data from a webform submission.
 *
 * @param int $sid
 *   The webform submission id.
 *
 * @return array
 *   The submitted data from specific $sid.
 */
function _webform2sftp_get_submission_data_to_export($sid) {
  $query = db_select('webform_submitted_data');

  $query->join('webform2sftp_components_order', 'components_order',
    'webform_submitted_data.cid = components_order.cid AND
    webform_submitted_data.nid = components_order.nid');

  $query->fields('webform_submitted_data', array('nid', 'cid', 'data'))
    ->condition('sid', $sid, '=')
    ->condition('components_order.disabled', 0, '=')
    ->orderBy('components_order.weight', 'ASC');

  $query_result = $query->execute();

  while ($record = $query_result->fetchAssoc()) {
    $webform2sftp_data[$record['cid']] = _webform2sftp_decrypt_webform_data_if_configured($record['data'], $record['nid'], $record['cid']);
  }

  if (!isset($webform2sftp_data)) {
    watchdog('webform2sftp', 'There were no fields enabled for exporting', array(), WATCHDOG_ERROR);
    drupal_set_message(t('There are no fields enabled for exporting.'), 'error');

    $webform2sftp_data = FALSE;
  }

  return $webform2sftp_data;
}

/**
 * Decrypts submission data encrypted using webform encrypt.
 *
 * @param string $data
 *   An encrypted string if component encryption is enabled
 *   A string, otherwise.
 * @param string $nid
 *   Node id for the webform.
 * @param string $cid
 *   Component id for the actual webform component.
 *
 * @return string
 *   Own data if decryption flag is not set
 *   Decrypted data, otherwise.
 */
function _webform2sftp_decrypt_webform_data_if_configured($data, $nid, $cid) {
  module_load_include('inc', 'webform2sftp', 'inc/webform2sftp.common');

  if (webform2sftp_is_to_decrypt_webform_data() && webform2sftp_component_uses_encryption($nid, $cid)) {
    $data = decrypt($data, array('base64' => TRUE));
  }

  return $data;
}

/**
 * Checks if the webform component is encrypted.
 *
 * @param string $nid
 *   Node id for the webform.
 * @param string $cid
 *   Component id for the actual webform component.
 *
 * @return bool
 *   True if value is encrypted
 *   False, otherwise.
 */
function webform2sftp_component_uses_encryption($nid, $cid) {
  $query = db_select('webform_component');

  $query->fields('webform_component', array('extra'))
    ->condition('nid', $nid, '=')
    ->condition('cid', $cid, '=');

  $result = $query->execute()->fetchAssoc();

  $component_extra_info = unserialize($result['extra']);
  $component_encryption = isset($component_extra_info['encrypt']) ? $component_extra_info['encrypt'] : 0;

  return $component_encryption;
}

/**
 * Generates file with webform node submissions.
 *
 * @param array $submissions
 *   An array of webform submissions.
 * @param string $file_name_prefix
 *   The given file name prefix.
 *
 * @return objectNULL
 *   NULL, if file could not be generated
 *   File object, otherwise.
 */
function webform2sftp_generate_files(array $submissions, $file_name_prefix) {
  module_load_include('inc', 'webform2sftp', 'inc/webform2sftp.common');

  if (in_array(FALSE, $submissions, TRUE)) {
    return $generated_files = NULL;
  }

  $generated_files['data'] = webform2sftp_generate_data_file($submissions, $file_name_prefix);

  if (webform2sftp_is_to_generate_trigger_file()) {
    $generated_files['trigger'] = webform2sftp_generate_trigger_file($submissions, $file_name_prefix);
  }

  if (!in_array(NULL, $generated_files)) {
    watchdog('webform2sftp', 'The flat file(s) was successfully generated in the folder: temporary://');
  }
  else {
    $generated_files = array();
  }

  return $generated_files;
}

/**
 * Returns the path of a generated file containing webform submissions data.
 *
 * @param array $submissions
 *   An array of webform submissions data.
 * @param string $file_name_prefix
 *   The file name prefix.
 *
 * @return string
 *   The generated file path.
 */
function webform2sftp_generate_data_file(array $submissions, $file_name_prefix) {
  $saved_file_path = NULL;
  $date = date('ymdHis');

  $file_name = $file_name_prefix . '.' . $date . '.txt';
  $file_data = NULL;

  $line_ending = webform2sftp_get_line_ending();
  foreach ($submissions as $submission) {
    $file_data .= webform2sftp_get_file_row($submission) . $line_ending;
  }

  if ($saved_file = file_save_data($file_data, 'temporary://' . $file_name, FILE_EXISTS_REPLACE)) {
    $saved_file_path = drupal_realpath($saved_file->uri);
  }

  return $saved_file_path;
}

/**
 * Returns one row as string to be print in the export file.
 *
 * @param array $submission
 *   The submission data to be printed as a string.
 *
 * @return string
 *   The submission fields string delimited by $delimiter
 */
function webform2sftp_get_file_row(array $submission) {
  module_load_include('inc', 'webform2sftp', 'inc/webform2sftp.common');

  $delimiter = webform2sftp_get_delimiter_character();

  $row = NULL;

  $submission = array_values($submission);
  foreach ($submission as $key => $submission_data_field) {
    if ($key == 0) {
      $row .= $submission_data_field . "";
    }
    else {
      $row .= $delimiter . $submission_data_field . "";
    }
  }

  return $row;
}

/**
 * Generates trigger file. It's a file with information about data file (txt).
 *
 * @param array $submissions
 *   An array of webform submissions.
 * @param string $file_name_prefix
 *   The file name prefix.
 *
 * @return object
 *   NULL, in case of fail to generate trigger File
 *   File Object, otherwise.
 */
function webform2sftp_generate_trigger_file(array $submissions, $file_name_prefix) {
  module_load_include('inc', 'webform2sftp', 'inc/webform2sftp.common');

  $saved_file_path = NULL;

  $date = date('ymdHis');
  $file_data = NULL;
  $delimiter = webform2sftp_get_delimiter_character();

  $number_of_submissions = count($submissions);

  $file_data = $file_name_prefix . '.' . $date . '.txt' . $delimiter . WEBFORM2SFTP_NUMBER_GENERATED_FILES_PER_NODE . $delimiter . $number_of_submissions;
  $file_name = $file_name_prefix . '.' . $date . '.trg';
  if ($saved_file = file_save_data($file_data, 'temporary://' . $file_name, FILE_EXISTS_REPLACE)) {
    $saved_file_path = drupal_realpath($saved_file->uri);
  }

  return $saved_file_path;
}
