<?php

/**
 * @file
 * Webform2sftp classes implementation.
 */

/**
 * Webform2sftp PGP Encrypt Class.
 */
class Webform2SftpPGP {
  const EXTENSION_GNUPG = 'gnupg';

  /**
   * Encrypts the given file content using PGP.
   *
   * @param string $file
   *   The file to be encrypted.
   * @param string $public_key
   *   The public key path.
   *
   * @return bool
   *   The file details in case of success, FALSE otherwise.
   */
  public static function encryptFileContent($file, $public_key) {
    $encrypted_content = NULL;

    $gnupg = self::gnupgCreateNewInstance($public_key);
    if ($gnupg) {
      $encrypted_content = $gnupg->encrypt(file_get_contents($file));
    }

    return $encrypted_content;
  }

  /**
   * Returns a new pgp instantiated object.
   *
   * @param string $public_key
   *   The public key path.
   */
  protected function gnupgCreateNewInstance($public_key) {
    $gnupg = NULL;

    if (extension_loaded(self::EXTENSION_GNUPG)) {
      putenv('GNUPGHOME=' . variable_get('file_temporary_path', '/tmp'));

      $gnupg = new gnupg();

      // Set errormode to PHP WARNING rather than raise an exception.
      $gnupg->seterrormode(GNUPG_ERROR_WARNING);

      // Import key at every encryption to not rely on GPG.
      $public_key_info = $gnupg->import(file_get_contents($public_key));
      $gnupg->addencryptkey($public_key_info['fingerprint']);

      // setarmor=1 will include header information at the content.
      $gnupg->setarmor(0);
    }

    return $gnupg;
  }

}

/**
 * Webform2sftp SSH2 Class.
 */
class Webform2SftpSSH2 {
  public $sftpAddress;
  public $sftpUser;
  public $sftpPassword;
  public $sftpPublicKey;
  public $sftpPrivateKey;

  const SFTP_PORT = 22;

  /**
   * Implements method __construct().
   */
  public function __construct($sftp_address, $sftp_user, $sftp_password, $sftp_public_key = NULL, $sftp_private_key = NULL) {
    $this->sftpAddress = $sftp_address;
    $this->sftpUser = $sftp_user;
    $this->sftpPassword = $sftp_password;
    $this->sftpPublicKey = $sftp_public_key;
    $this->sftpPrivateKey = $sftp_private_key;
  }

  /**
   * Sends the given file to a SFTP repository.
   *
   * @param string $file
   *   The file that will be sent.
   * @param string $sftp_repository
   *   The sFTP repository path.
   *
   * @return bool
   *   TRUE in case of success, FALSE otherwise.
   */
  public function sftpSendFile($file, $sftp_repository) {
    $is_successful = FALSE;

    $resource = $this->ssh2CreateConnection();
    if ($resource) {
      $sftp_stream = $this->openSftpStream($resource, basename($file), $sftp_repository);

      try {
        $this->sftpWriteFile($sftp_stream, $file);
        $is_successful = TRUE;
      }
      catch (Exception $e) {
        watchdog('webform2sftp', 'Exception: @exception', array('@exception' => $e->getMessage()));
      }

      $this->closeSftpStream($sftp_stream);
    }

    return $is_successful;
  }

  /**
   * Write the file in remote sftp server.
   *
   * @param object $sftp_stream
   *   A sftp stream session.
   * @param string $file
   *   The given file.
   *
   * @throws Exception
   */
  protected function sftpWriteFile($sftp_stream, $file) {
    if (!$sftp_stream) {
      throw new Exception('Could not open remote file.');
    }

    $data_to_send = file_get_contents($file);
    if ($data_to_send === FALSE) {
      throw new Exception("Could not open local file: $file.");
    }

    if (fwrite($sftp_stream, $data_to_send) === FALSE) {
      throw new Exception("Could not send data from file: $file.");
    }
  }

  /**
   * Establish a connection to a remote SSH server.
   *
   * @return bool
   *   TRUE in case of success, FALSE otherwise.
   */
  protected function ssh2CreateConnection() {
    $resource = ssh2_connect($this->sftpAddress, self::SFTP_PORT);
    if ($resource && !$this->ssh2Auth($resource)) {
      $resource = FALSE;
    }

    return $resource;
  }

  /**
   * Provides SSH2 authentication.
   */
  protected function ssh2Auth($resource) {
    $auth = FALSE;

    if (!empty($this->sftpPublicKey) && !empty($this->sftpPrivateKey)) {
      // Authenticate using a public key.
      $auth = ssh2_auth_pubkey_file(
        $resource,
        $this->sftpUser,
        $this->sftpPublicKey,
        $this->sftpPrivateKey,
        $this->sftpPassword
      );
    }
    else {
      // Authenticate over SSH using a plain password.
      $auth = ssh2_auth_password($resource, $this->sftpUser, $this->sftpPassword);
    }

    return $auth;
  }

  /**
   * Binds a named resource, specified by file name, to a stream.
   */
  protected function openSftpStream($resource, $file_name, $sftp_repository) {
    $sftp = $this->initializeSftpSubsystem($resource);

    return fopen('ssh2.sftp://' . intval($sftp) . '/' . $sftp_repository . '/' . $file_name, 'w');
  }

  /**
   * Request the SFTP subsystem from an already connected SSH2 server.
   *
   * @param object $resource
   *   SSH connection link identifier, obtained from a call to ssh2_connect().
   */
  protected function initializeSftpSubsystem($resource) {
    return ssh2_sftp($resource);
  }

  /**
   * Closes the opened SFTP file pointer.
   *
   * @param object $stream
   *   A sftp stream session.
   */
  protected function closeSftpStream($stream) {
    fclose($stream);
  }

}
